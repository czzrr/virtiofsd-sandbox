# virtiofsd-sandbox

## Usage
The tool currently runs `virtiofsd --sandbox none` in a user namespace.

Example command:

```
cargo run -- --virtiofsd-path /home/ca/work/virtiofsd/target/debug/virtiofsd --shared-dir . --socket-path /tmp/vfsd.sock
```

### Flags

```
--virtiofsd-path <virtiofsd-path>
```
virtiofsd executable path.

```
--shared-dir <shared-dir>
```
Shared directory path.

```
--uid-map :namespace_uid:host_uid:count:
```
Map a range of UIDs from host to namespace.

```
--gid-map :namespace_gid:host_gid:count:
```
Map a range of GIDs from host to namespace.

```
-- <virtiofsd-args>
```
Arguments to virtiofsd (must be specified last).
`--shared-dir` must only be specified once as an argument to virtiofsd-sandbox.