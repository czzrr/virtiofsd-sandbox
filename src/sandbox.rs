use vhost::vhost_user::Listener;

use crate::{
    idmap::{GidMap, IdMapSetUpPipeMessage, UidMap},
    oslib, util,
};
use std::{
    error,
    ffi::CString,
    fmt,
    io::{self, Read, Write},
    os::fd::AsRawFd,
    path::Path,
    process::{self, Command},
};

#[derive(Debug)]
pub enum Error {
    /// Failed to bind mount `/proc/self/fd` into a temporary directory.
    BindMountProcSelfFd(io::Error),
    /// Failed to bind mount shared directory.
    BindMountSharedDir(io::Error),
    /// Failed to change to the old root directory.
    ChdirOldRoot(io::Error),
    /// Failed to change to the new root directory.
    ChdirNewRoot(io::Error),
    /// Call to libc::chroot returned an error.
    Chroot(io::Error),
    /// Failed to change to the root directory after the chroot call.
    ChrootChdir(io::Error),
    /// Failed to clean the properties of the mount point.
    CleanMount(io::Error),
    /// Failed to create a temporary directory.
    CreateTempDir(io::Error),
    /// Failed to drop supplemental groups.
    DropSupplementalGroups(io::Error),
    /// Call to libc::fork returned an error.
    Fork(io::Error),
    /// Failed to get the number of supplemental groups.
    GetSupplementalGroups(io::Error),
    /// Error bind-mounting a directory.
    MountBind(io::Error),
    /// Failed to mount old root.
    MountOldRoot(io::Error),
    /// Error mounting proc.
    MountProc(io::Error),
    /// Failed to mount new root.
    MountNewRoot(io::Error),
    /// Error mounting target directory.
    MountTarget(io::Error),
    /// Error mounting tmpfs.
    MountTmpFs(io::Error),
    /// Error mounting lib.
    MountLib(io::Error),
    /// Error mounting lib64.
    MountLib64(io::Error),
    /// Failed to open `/proc/self/mountinfo`.
    OpenMountinfo(io::Error),
    /// Failed to open new root.
    OpenNewRoot(io::Error),
    /// Failed to open old root.
    OpenOldRoot(io::Error),
    /// Failed to open `/proc/self`.
    OpenProcSelf(io::Error),
    /// Failed to open `/proc/self/fd`.
    OpenProcSelfFd(io::Error),
    /// Error switching root directory.
    PivotRoot(io::Error),
    /// Failed to remove temporary directory.
    RmdirTempDir(io::Error),
    /// Failed to lazily unmount old root.
    UmountOldRoot(io::Error),
    /// Failed to lazily unmount temporary directory.
    UmountTempDir(io::Error),
    /// Call to libc::unshare returned an error.
    Unshare(io::Error),
    /// Failed to execute `newgidmap(1)`.
    WriteGidMap(String),
    /// Failed to write to `/proc/self/setgroups`.
    WriteSetGroups(io::Error),
    /// Failed to execute `newuidmap(1)`.
    WriteUidMap(String),
    /// Sandbox mode unavailable for non-privileged users
    SandboxModeInvalidUID,
    /// Setting uid_map is only allowed inside a namespace for non-privileged users
    SandboxModeInvalidUidMap,
    /// Setting gid_map is only allowed inside a namespace for non-privileged users
    SandboxModeInvalidGidMap,
    /// Failed to execute virtiofsd
    VirtiofsdExec(String),
}

impl error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use self::Error::{
            SandboxModeInvalidGidMap, SandboxModeInvalidUID, SandboxModeInvalidUidMap, WriteGidMap,
            WriteUidMap,
        };
        match self {
            SandboxModeInvalidUID => {
                write!(
                    f,
                    "sandbox mode 'chroot' can only be used by \
                    root (Use '--sandbox namespace' instead)"
                )
            }
            SandboxModeInvalidUidMap => {
                write!(
                    f,
                    "uid_map can only be used by unprivileged user where sandbox mod is namespace \
                    (Use '--sandbox namespace' instead)"
                )
            }
            SandboxModeInvalidGidMap => {
                write!(
                    f,
                    "gid_map can only be used by unprivileged user where sandbox mod is namespace \
                    (Use '--sandbox namespace' instead)"
                )
            }
            WriteUidMap(msg) => write!(f, "write to uid map failed: {msg}"),
            WriteGidMap(msg) => write!(f, "write to gid map failed: {msg}"),
            _ => write!(f, "{self:?}"),
        }
    }
}

pub struct Sandbox {
    virtiofsd_path: String,
    socket_path: String,
    shared_dir: String,
    uid_map: Option<UidMap>,
    gid_map: Option<GidMap>,
    virtiofsd_args: Vec<String>,
}

impl Sandbox {
    pub fn new(
        virtiofsd_path: String,
        socket_path: String,
        shared_dir: String,
        uid_map: Option<UidMap>,
        gid_map: Option<GidMap>,
        virtiofsd_args: Vec<String>,
    ) -> Sandbox {
        Sandbox {
            virtiofsd_path,
            socket_path,
            shared_dir,
            uid_map,
            gid_map,
            virtiofsd_args,
        }
    }

    pub fn enter(&self) -> Result<(), Error> {
        // Create pipes for synchronization between parent and child processes.
        let (mut x_reader, mut x_writer) = oslib::pipe().unwrap();
        let (mut y_reader, mut y_writer) = oslib::pipe().unwrap();

        let pid = util::sfork().map_err(Error::Fork)?;
        let mut output = [0];

        if pid == 0 {
            // Child.
            // Sets up ID mappings for the parent process in the new user namespace,
            // allowing it to have arbitrary mappings (with respect to /proc/[pid]/uid_map and /proc/[pid]/gid_map).

            // Drop unneeded ends of pipes.
            drop(x_writer);
            drop(y_reader);

            // Wait until unshare() returns.
            x_reader.read_exact(&mut output).unwrap();
            assert_eq!(output[0], IdMapSetUpPipeMessage::Request as u8);

            let ppid = unsafe { libc::getppid() };
            self.setup_id_mappings(ppid)?;

            y_writer
                .write_all(&[IdMapSetUpPipeMessage::Done as u8])
                .unwrap();

            process::exit(0);
        } else {
            // Parent.

            // Drop unneeded ends of pipes.
            drop(x_reader);
            drop(y_writer);

            let (_listener, socket_listener_fd, virtiofsd_fd) = self.get_fds()?;

            // Unshare namespaces. This places the current process in a new user and mount namespace.
            // Only child processes are placed in the new PID namespace.
            unsafe { libc::unshare(libc::CLONE_NEWUSER | libc::CLONE_NEWNS | libc::CLONE_NEWPID) };

            // Signal that unshare() has returned.
            x_writer
                .write_all(&[IdMapSetUpPipeMessage::Request as u8])
                .unwrap();

            // Wait until ID mappings have been set by child.
            y_reader.read_exact(&mut output).unwrap();
            assert_eq!(output[0], IdMapSetUpPipeMessage::Done as u8);

            // Set the process inside the user namespace as root
            let mut ret = unsafe { libc::setresuid(0, 0, 0) };
            if ret != 0 {
                log::warn!("Couldn't set the process uid as root: {}", ret);
            }
            ret = unsafe { libc::setresgid(0, 0, 0) };
            if ret != 0 {
                log::warn!("Couldn't set the process gid as root: {}", ret);
            }

            // Fork to create a child process in the new PID namespace.
            let pid = unsafe { libc::fork() };
            if pid == 0 {
                // Child.

                let shared_dir_file_name = self.setup_mounts()?;
                self.spawn_virtiofsd(socket_listener_fd, virtiofsd_fd, &shared_dir_file_name)?;
            } else {
                // Parent.

                let mut status = 0;
                // Never returns.
                unsafe { libc::waitpid(pid, &mut status, 0) };
            }
        }

        Ok(())
    }

    fn spawn_virtiofsd(
        &self,
        socket_listener_fd: i32,
        virtiofsd_fd: i32,
        shared_dir_file_name: &str,
    ) -> Result<(), Error> {
        // Create a pipe for virtiofsd to tell us when it has started.
        let (mut reader, writer) = oslib::pipe().unwrap();

        let pid = util::sfork().map_err(Error::Fork)?;
        if pid == 0 {
            // Child.

            // Drop read end of pipe so we are notified if the pipe is broken, i.e., the parent died.
            drop(reader);

            // Get file descriptors for /proc/self/fd and /proc/self/mountinfo.
            let c_proc_pid_fd_path = CString::new("/proc/self/fd").unwrap();
            let proc_pid_fd_fd = unsafe { libc::open(c_proc_pid_fd_path.as_ptr(), libc::O_PATH) };
            if proc_pid_fd_fd < 0 {
                return Err(Error::OpenProcSelfFd(io::Error::last_os_error()));
            }
            let cproc_pid_mountinfo_fd_path = CString::new("/proc/self/mountinfo").unwrap();
            let proc_pid_mountinfo_fd =
                unsafe { libc::open(cproc_pid_mountinfo_fd_path.as_ptr(), libc::O_PATH) };
                if proc_pid_mountinfo_fd < 0 {
                    return Err(Error::OpenProcSelfFd(io::Error::last_os_error()));
                }

            assert_eq!(unsafe { libc::fcntl(proc_pid_fd_fd, libc::F_GETFD) }, 0);
            assert_eq!(unsafe { libc::fcntl(proc_pid_mountinfo_fd, libc::F_GETFD) }, 0);

            self.exec_virtiofsd(
                socket_listener_fd,
                virtiofsd_fd,
                shared_dir_file_name,
                writer.as_raw_fd(),
                proc_pid_fd_fd,
                proc_pid_mountinfo_fd,
            );
        } else {
            // Parent.

            // Drop write end of pipe so we are notified if the pipe is broken, i.e., the child died.
            drop(writer);

            // Wait until virtiofsd has started.
            let mut output = [0];
            reader.read_exact(&mut output).unwrap();
            assert_eq!(output[0], IdMapSetUpPipeMessage::Done as u8);
            log::debug!("Received message from virtiofsd that it has started");

            // Unmount virtiofsd dependencies now that it's running.
            oslib::umount2("/lib", libc::MNT_DETACH).unwrap();
            oslib::umount2("/lib64", libc::MNT_DETACH).unwrap();
            oslib::umount2("/proc", libc::MNT_DETACH).unwrap();

            std::fs::remove_dir("/lib").unwrap();
            std::fs::remove_dir("/lib64").unwrap();
            std::fs::remove_dir("/proc").unwrap();

            // Remount new root as read-only.
            oslib::mount(None, ".", None, libc::MS_BIND | libc::MS_REC | libc::MS_REMOUNT | libc::MS_RDONLY).map_err(Error::CleanMount)?;

            //print_contents("/");

            let mut status = 0;
            unsafe { libc::waitpid(pid, &mut status, 0) };
        }

        Ok(())
    }

    fn exec_virtiofsd(
        &self,
        socket_listener_fd: i32,
        virtiofsd_fd: i32,
        shared_dir_file_name: &str,
        writer_fd: i32,
        proc_self_fd_fd: i32,
        proc_self_mountinfo_fd: i32,
    ) {
        // Unset FD_CLOEXEC flag of `writer_fd`.
        unsafe { libc::fcntl(writer_fd, libc::F_SETFD, 0) };
        assert_eq!(unsafe { libc::fcntl(writer_fd, libc::F_GETFD) }, 0);

        let socket_listener_fd_str = socket_listener_fd.to_string();
        let writer_fd_str = writer_fd.to_string();
        let proc_self_fd_fd_str = proc_self_fd_fd.to_string();
        let proc_self_mountinfo_fd_str = proc_self_mountinfo_fd.to_string();

        let mut virtiofsd_args = Vec::new();
        virtiofsd_args.push(self.virtiofsd_path.as_str());
        virtiofsd_args.push("--sandbox");
        virtiofsd_args.push("none");
        virtiofsd_args.extend(self.virtiofsd_args.iter().map(|s| s.as_str()));
        virtiofsd_args.push("--shared-dir");
        virtiofsd_args.push(shared_dir_file_name);
        virtiofsd_args.push("--fd");
        virtiofsd_args.push(&socket_listener_fd_str);
        virtiofsd_args.push("--sandbox-pipe-fd");
        virtiofsd_args.push(&writer_fd_str);
        virtiofsd_args.push("--proc-self-fd");
        virtiofsd_args.push(&proc_self_fd_fd_str);
        virtiofsd_args.push("--proc-self-mountinfo");
        virtiofsd_args.push(&proc_self_mountinfo_fd_str);

        log::debug!("virtiofsd arguments: {:?}", virtiofsd_args);

        // Transform arguments into CString's.
        let virtiofsd_args: Vec<CString> = virtiofsd_args
            .into_iter()
            .map(|s| CString::new(s).expect(&format!("Invalid argument: {}", s)))
            .collect();

        // Collect pointers of arguments.
        let mut virtiofsd_args: Vec<*const i8> =
            virtiofsd_args.iter().map(|s| s.as_ptr()).collect();
        virtiofsd_args.push(std::ptr::null());

        // Execute virtiofsd.
        let res = unsafe {
            libc::fexecve(
                virtiofsd_fd,
                virtiofsd_args.as_ptr(),
                [std::ptr::null()].as_ptr(),
            )
        };
        unreachable!(
            "fexecve() error (returned {}): {}",
            res,
            io::Error::last_os_error()
        );
    }

    fn setup_mounts(&self) -> Result<String, Error> {
        // Mark the root directory as slave recursively so its mounts propagate to us, but our mounts do not propagate to it.
        oslib::mount(None, "/", None, libc::MS_SLAVE | libc::MS_REC).map_err(Error::CleanMount)?;

        // Create a tmpfs which we will use as the new root filesystem.
        oslib::mount(
            "tmpfs".into(),
            "/tmp",
            "tmpfs".into(),
            libc::MS_NODEV | libc::MS_NOSUID,
        )
        .map_err(Error::MountTmpFs)?;

        // Get absolute shared directory path before we change directory in case the path is relative.
        let shared_dir_abs_path = Path::new(&self.shared_dir).canonicalize().unwrap();

        // Switch our working directory to the new tmpfs.
        std::env::set_current_dir("/tmp").unwrap();

        // Make shared directory.
        let shared_dir_file_name = shared_dir_abs_path
            .file_name()
            .unwrap()
            .to_string_lossy()
            .to_string();
        std::fs::create_dir(&shared_dir_file_name).unwrap();
        // Make lib directory.
        std::fs::create_dir("lib").unwrap();
        // Make lib64 directory.
        std::fs::create_dir("lib64").unwrap();
        // Make proc directory.
        std::fs::create_dir("proc").unwrap();

        // Bind mount the shared directory in the tmpfs.
        let shared_dir_abs_path = shared_dir_abs_path.to_string_lossy().to_string();
        oslib::mount(
            shared_dir_abs_path.as_str().into(),
            &shared_dir_file_name,
            None,
            libc::MS_BIND | libc::MS_REC,
        )
        .unwrap();

        // Bind mount /lib.
        oslib::mount("/lib".into(), "lib", None, libc::MS_BIND | libc::MS_REC)
            .map_err(Error::MountLib)?;

        // Bind mount /lib64.
        oslib::mount("/lib64".into(), "lib64", None, libc::MS_BIND | libc::MS_REC)
            .map_err(Error::MountLib64)?;

        // Get a file descriptor to our old root so we can reference it after switching root.
        let c_root_dir = CString::new("/").unwrap();
        let oldroot_fd = unsafe {
            libc::open(
                c_root_dir.as_ptr(),
                libc::O_DIRECTORY | libc::O_RDONLY | libc::O_CLOEXEC,
            )
        };
        if oldroot_fd < 0 {
            return Err(Error::OpenOldRoot(std::io::Error::last_os_error()));
        }

        // Get a file descriptor to the new root so we can reference it after switching root.
        let c_current_dir = CString::new(".").unwrap();
        let newroot_fd = unsafe {
            libc::open(
                c_current_dir.as_ptr(),
                libc::O_DIRECTORY | libc::O_RDONLY | libc::O_CLOEXEC,
            )
        };
        if newroot_fd < 0 {
            return Err(Error::OpenNewRoot(std::io::Error::last_os_error()));
        }

        // Our current working directory is `/tmp`.
        // Call to `pivot_root` using `.` as both new and old root.
        let c_current_dir = CString::new(".").unwrap();
        let ret = unsafe {
            libc::syscall(
                libc::SYS_pivot_root,
                c_current_dir.as_ptr(),
                c_current_dir.as_ptr(),
            )
        };
        if ret < 0 {
            return Err(Error::PivotRoot(std::io::Error::last_os_error()));
        }

        // Change to new root.
        oslib::fchdir(newroot_fd).map_err(Error::ChdirNewRoot)?;

        // Mount proc.
        oslib::mount(
            "proc".into(),
            "/proc",
            "proc".into(),
            libc::MS_NODEV | libc::MS_NOEXEC | libc::MS_NOSUID | libc::MS_RELATIME,
        )
        .map_err(Error::MountProc)?;

        // Change to old root directory to prepare for cleaning up and unmounting it.
        oslib::fchdir(oldroot_fd).map_err(Error::ChdirOldRoot)?;

        // Set the mount propagation type of old root to `slave` to avoid our mount events propagate to it.
        oslib::mount(None, ".", None, libc::MS_SLAVE | libc::MS_REC).map_err(Error::CleanMount)?;
        
        // Lazily unmount old root.
        oslib::umount2(".", libc::MNT_DETACH).map_err(Error::UmountOldRoot)?;

        // Change to new root.
        oslib::fchdir(newroot_fd).map_err(Error::ChdirNewRoot)?;

        // We no longer need these file descriptors, so close them.
        unsafe { libc::close(newroot_fd) };
        unsafe { libc::close(oldroot_fd) };

        Ok(shared_dir_file_name)
    }

    fn get_fds(&self) -> Result<(Listener, i32, i32), Error> {
        // Obtain file descriptor for socket listener.
        let socket_listener =
            Listener::new(self.socket_path.clone(), true).expect("Error creating socket listener");
        let socket_listener_fd = socket_listener.as_raw_fd();
        if socket_listener_fd == -1 {
            panic!("socket_listener_fd error: {}", io::Error::last_os_error());
        }

        // Unset FD_CLOEXEC so the socket listener stays alive across exec().
        unsafe { libc::fcntl(socket_listener_fd, libc::F_SETFD, 0) };
        // Check that FD_CLOEXEC is unset.
        assert!(unsafe { libc::fcntl(socket_listener_fd, libc::F_GETFD) } == 0);

        // Obtain file descriptor for virtiofsd executable.
        let c_virtiofsd_path =
            CString::new(self.virtiofsd_path.clone()).expect("Invalid virtiofsd path");
        let virtiofsd_fd = unsafe { libc::open(c_virtiofsd_path.as_ptr(), libc::O_RDONLY) };
        if virtiofsd_fd == -1 {
            panic!("virtiofsd_fd error: {}", io::Error::last_os_error());
        }

        Ok((socket_listener, socket_listener_fd, virtiofsd_fd))
    }

    fn setup_id_mappings(&self, pid: i32) -> Result<(), Error> {
        let mut newuidmap = Command::new("newuidmap");
        newuidmap.arg(pid.to_string());
        if let Some(ref uid_map) = self.uid_map {
            newuidmap.arg(uid_map.inside_uid.to_string());
            newuidmap.arg(uid_map.outside_uid.to_string());
            newuidmap.arg(uid_map.count.to_string());
        } else {
            // Set up 1-to-1 mappings for our current uid.
            let current_uid = unsafe { libc::geteuid() };
            newuidmap.arg(current_uid.to_string());
            newuidmap.arg(current_uid.to_string());
            newuidmap.arg("1");
        }
        let mut output = newuidmap.output().map_err(|_| {
            Error::WriteUidMap(format!(
                "failed to execute newuidmap: {}",
                io::Error::last_os_error()
            ))
        })?;
        if !output.status.success() {
            return Err(Error::WriteUidMap(
                String::from_utf8_lossy(&output.stderr).to_string(),
            ));
        }

        let mut newgidmap = Command::new("newgidmap");
        newgidmap.arg(pid.to_string());
        if let Some(ref gid_map) = self.gid_map {
            newgidmap.arg(gid_map.inside_gid.to_string());
            newgidmap.arg(gid_map.outside_gid.to_string());
            newgidmap.arg(gid_map.count.to_string());
        } else {
            // Set up 1-to-1 mappings for our current gid.
            let current_gid = unsafe { libc::getegid() };
            newgidmap.arg(current_gid.to_string());
            newgidmap.arg(current_gid.to_string());
            newgidmap.arg("1");
        }
        output = newgidmap.output().map_err(|_| {
            Error::WriteGidMap(format!(
                "failed to execute newgidmap: {}",
                io::Error::last_os_error()
            ))
        })?;
        if !output.status.success() {
            return Err(Error::WriteGidMap(
                String::from_utf8_lossy(&output.stderr).to_string(),
            ));
        }
        Ok(())
    }
}

fn print_contents(path: &str) {
    println!("Contents of {}:", path);
    for path in std::fs::read_dir(path).unwrap() {
        println!("Name: {}", path.unwrap().path().display())
    }
}
